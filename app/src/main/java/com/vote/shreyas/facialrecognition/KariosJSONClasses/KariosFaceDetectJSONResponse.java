package com.vote.shreyas.facialrecognition.KariosJSONClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Dr.Xperience on 019-2017-09-19.
 */

public class KariosFaceDetectJSONResponse
{

	@SerializedName("images")
	@Expose
	private List<Images> images;

	public List<Images> getImages()
	{
		return images;
	}

	public void setImages(List<Images> images)
	{
		this.images = images;
	}

	public static class Images
	{
		/**
		 * status : Complete
		 * width : 1536
		 * height : 2048
		 * file : kairos-elizabeth.jpg
		 * faces : [{"topLeftX":390,"topLeftY":706,"chinTipX":780,"rightEyeCenterX":587,"yaw":-3,"chinTipY":1548,"confidence":0.99997,"height":780,"rightEyeCenterY":904,"width":781,"leftEyeCenterY":907,"leftEyeCenterX":955,"pitch":-17,"attributes":{"lips":"Together","asian":0.25658,"gender":{"type":"F"},"age":26,"hispanic":0.41825,"other":0.11144,"black":0.16007,"white":0.05365,"glasses":"None"},"face_id":1,"quality":0.79333,"roll":-1}]
		 */

		@SerializedName("status")
		private String status;
		@SerializedName("width")
		private int width;
		@SerializedName("height")
		private int height;
		@SerializedName("file")
		private String file;
		@SerializedName("faces")
		private List<Faces> faces;

		public String getStatus()
		{
			return status;
		}

		public void setStatus(String status)
		{
			this.status = status;
		}

		public int getWidth()
		{
			return width;
		}

		public void setWidth(int width)
		{
			this.width = width;
		}

		public int getHeight()
		{
			return height;
		}

		public void setHeight(int height)
		{
			this.height = height;
		}

		public String getFile()
		{
			return file;
		}

		public void setFile(String file)
		{
			this.file = file;
		}

		public List<Faces> getFaces()
		{
			return faces;
		}

		public void setFaces(List<Faces> faces)
		{
			this.faces = faces;
		}

		public static class Faces
		{
			/**
			 * topLeftX : 390
			 * topLeftY : 706
			 * chinTipX : 780
			 * rightEyeCenterX : 587
			 * yaw : -3
			 * chinTipY : 1548
			 * confidence : 0.99997
			 * height : 780
			 * rightEyeCenterY : 904
			 * width : 781
			 * leftEyeCenterY : 907
			 * leftEyeCenterX : 955
			 * pitch : -17
			 * attributes : {"lips":"Together","asian":0.25658,"gender":{"type":"F"},"age":26,"hispanic":0.41825,"other":0.11144,"black":0.16007,"white":0.05365,"glasses":"None"}
			 * face_id : 1
			 * quality : 0.79333
			 * roll : -1
			 */

			@SerializedName("topLeftX")
			private int topLeftX;
			@SerializedName("topLeftY")
			private int topLeftY;
			@SerializedName("chinTipX")
			private int chinTipX;
			@SerializedName("rightEyeCenterX")
			private int rightEyeCenterX;
			@SerializedName("yaw")
			private int yaw;
			@SerializedName("chinTipY")
			private int chinTipY;
			@SerializedName("confidence")
			private double confidence;
			@SerializedName("height")
			private int height;
			@SerializedName("rightEyeCenterY")
			private int rightEyeCenterY;
			@SerializedName("width")
			private int width;
			@SerializedName("leftEyeCenterY")
			private int leftEyeCenterY;
			@SerializedName("leftEyeCenterX")
			private int leftEyeCenterX;
			@SerializedName("pitch")
			private int pitch;
			@SerializedName("attributes")
			private Attributes attributes;
			@SerializedName("face_id")
			private int faceId;
			@SerializedName("quality")
			private double quality;
			@SerializedName("roll")
			private int roll;

			public int getTopLeftX()
			{
				return topLeftX;
			}

			public void setTopLeftX(int topLeftX)
			{
				this.topLeftX = topLeftX;
			}

			public int getTopLeftY()
			{
				return topLeftY;
			}

			public void setTopLeftY(int topLeftY)
			{
				this.topLeftY = topLeftY;
			}

			public int getChinTipX()
			{
				return chinTipX;
			}

			public void setChinTipX(int chinTipX)
			{
				this.chinTipX = chinTipX;
			}

			public int getRightEyeCenterX()
			{
				return rightEyeCenterX;
			}

			public void setRightEyeCenterX(int rightEyeCenterX)
			{
				this.rightEyeCenterX = rightEyeCenterX;
			}

			public int getYaw()
			{
				return yaw;
			}

			public void setYaw(int yaw)
			{
				this.yaw = yaw;
			}

			public int getChinTipY()
			{
				return chinTipY;
			}

			public void setChinTipY(int chinTipY)
			{
				this.chinTipY = chinTipY;
			}

			public double getConfidence()
			{
				return confidence;
			}

			public void setConfidence(double confidence)
			{
				this.confidence = confidence;
			}

			public int getHeight()
			{
				return height;
			}

			public void setHeight(int height)
			{
				this.height = height;
			}

			public int getRightEyeCenterY()
			{
				return rightEyeCenterY;
			}

			public void setRightEyeCenterY(int rightEyeCenterY)
			{
				this.rightEyeCenterY = rightEyeCenterY;
			}

			public int getWidth()
			{
				return width;
			}

			public void setWidth(int width)
			{
				this.width = width;
			}

			public int getLeftEyeCenterY()
			{
				return leftEyeCenterY;
			}

			public void setLeftEyeCenterY(int leftEyeCenterY)
			{
				this.leftEyeCenterY = leftEyeCenterY;
			}

			public int getLeftEyeCenterX()
			{
				return leftEyeCenterX;
			}

			public void setLeftEyeCenterX(int leftEyeCenterX)
			{
				this.leftEyeCenterX = leftEyeCenterX;
			}

			public int getPitch()
			{
				return pitch;
			}

			public void setPitch(int pitch)
			{
				this.pitch = pitch;
			}

			public Attributes getAttributes()
			{
				return attributes;
			}

			public void setAttributes(Attributes attributes)
			{
				this.attributes = attributes;
			}

			public int getFaceId()
			{
				return faceId;
			}

			public void setFaceId(int faceId)
			{
				this.faceId = faceId;
			}

			public double getQuality()
			{
				return quality;
			}

			public void setQuality(double quality)
			{
				this.quality = quality;
			}

			public int getRoll()
			{
				return roll;
			}

			public void setRoll(int roll)
			{
				this.roll = roll;
			}

			public static class Attributes
			{
				/**
				 * lips : Together
				 * asian : 0.25658
				 * gender : {"type":"F"}
				 * age : 26
				 * hispanic : 0.41825
				 * other : 0.11144
				 * black : 0.16007
				 * white : 0.05365
				 * glasses : None
				 */

				@SerializedName("lips")
				private String lips;
				@SerializedName("asian")
				private double asian;
				@SerializedName("gender")
				private Gender gender;
				@SerializedName("age")
				private int age;
				@SerializedName("hispanic")
				private double hispanic;
				@SerializedName("other")
				private double other;
				@SerializedName("black")
				private double black;
				@SerializedName("white")
				private double white;
				@SerializedName("glasses")
				private String glasses;

				public String getLips()
				{
					return lips;
				}

				public void setLips(String lips)
				{
					this.lips = lips;
				}

				public double getAsian()
				{
					return asian;
				}

				public void setAsian(double asian)
				{
					this.asian = asian;
				}

				public Gender getGender()
				{
					return gender;
				}

				public void setGender(Gender gender)
				{
					this.gender = gender;
				}

				public int getAge()
				{
					return age;
				}

				public void setAge(int age)
				{
					this.age = age;
				}

				public double getHispanic()
				{
					return hispanic;
				}

				public void setHispanic(double hispanic)
				{
					this.hispanic = hispanic;
				}

				public double getOther()
				{
					return other;
				}

				public void setOther(double other)
				{
					this.other = other;
				}

				public double getBlack()
				{
					return black;
				}

				public void setBlack(double black)
				{
					this.black = black;
				}

				public double getWhite()
				{
					return white;
				}

				public void setWhite(double white)
				{
					this.white = white;
				}

				public String getGlasses()
				{
					return glasses;
				}

				public void setGlasses(String glasses)
				{
					this.glasses = glasses;
				}

				public static class Gender
				{
					/**
					 * type : F
					 */

					@SerializedName("type")
					private String type;

					public String getType()
					{
						return type;
					}

					public void setType(String type)
					{
						this.type = type;
					}
				}
			}
		}
	}
}
