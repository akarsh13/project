package com.vote.shreyas.facialrecognition.KariosJSONClasses;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Dr.Xperience on 019-2017-09-19.
 */

public class KariosFaceRecognizeJSONResponse
{

	@SerializedName("images")
	private List<Images> images;

	public List<Images> getImages()
	{
		return images;
	}

	public void setImages(List<Images> images)
	{
		this.images = images;
	}

	public static class Images
	{
		/**
		 * transaction : {"status":"success","width":175,"topLeftX":103,"topLeftY":157,"gallery_name":"MyGallery","face_id":1,"confidence":0.86944,"subject_id":"Elizabeth","height":175,"quality":0.84705}
		 * candidates : [{"subject_id":"Elizabeth","confidence":0.86944,"enrollment_timestamp":"1486925605094"},{"subject_id":"Rachel","confidence":0.63782,"enrollment_timestamp":"1416431816"}]
		 */

		@SerializedName("transaction")
		private Transaction transaction;
		@SerializedName("candidates")
		private List<Candidates> candidates;

		public Transaction getTransaction()
		{
			return transaction;
		}

		public void setTransaction(Transaction transaction)
		{
			this.transaction = transaction;
		}

		public List<Candidates> getCandidates()
		{
			return candidates;
		}

		public void setCandidates(List<Candidates> candidates)
		{
			this.candidates = candidates;
		}

		public static class Transaction
		{
			/**
			 * status : success
			 * width : 175
			 * topLeftX : 103
			 * topLeftY : 157
			 * gallery_name : MyGallery
			 * face_id : 1
			 * confidence : 0.86944
			 * subject_id : Elizabeth
			 * height : 175
			 * quality : 0.84705
			 */

			@SerializedName("status")
			private String status;
			@SerializedName("width")
			private int width;
			@SerializedName("topLeftX")
			private int topLeftX;
			@SerializedName("topLeftY")
			private int topLeftY;
			@SerializedName("gallery_name")
			private String galleryName;
			@SerializedName("face_id")
			private int faceId;
			@SerializedName("confidence")
			private double confidence;
			@SerializedName("subject_id")
			private String subjectId;
			@SerializedName("height")
			private int height;
			@SerializedName("quality")
			private double quality;

			public String getStatus()
			{
				return status;
			}

			public void setStatus(String status)
			{
				this.status = status;
			}

			public int getWidth()
			{
				return width;
			}

			public void setWidth(int width)
			{
				this.width = width;
			}

			public int getTopLeftX()
			{
				return topLeftX;
			}

			public void setTopLeftX(int topLeftX)
			{
				this.topLeftX = topLeftX;
			}

			public int getTopLeftY()
			{
				return topLeftY;
			}

			public void setTopLeftY(int topLeftY)
			{
				this.topLeftY = topLeftY;
			}

			public String getGalleryName()
			{
				return galleryName;
			}

			public void setGalleryName(String galleryName)
			{
				this.galleryName = galleryName;
			}

			public int getFaceId()
			{
				return faceId;
			}

			public void setFaceId(int faceId)
			{
				this.faceId = faceId;
			}

			public double getConfidence()
			{
				return confidence;
			}

			public void setConfidence(double confidence)
			{
				this.confidence = confidence;
			}

			public String getSubjectId()
			{
				return subjectId;
			}

			public void setSubjectId(String subjectId)
			{
				this.subjectId = subjectId;
			}

			public int getHeight()
			{
				return height;
			}

			public void setHeight(int height)
			{
				this.height = height;
			}

			public double getQuality()
			{
				return quality;
			}

			public void setQuality(double quality)
			{
				this.quality = quality;
			}
		}

		public static class Candidates
		{
			/**
			 * subject_id : Elizabeth
			 * confidence : 0.86944
			 * enrollment_timestamp : 1486925605094
			 */

			@SerializedName("subject_id")
			private String subjectId;
			@SerializedName("confidence")
			private double confidence;
			@SerializedName("enrollment_timestamp")
			private String enrollmentTimestamp;

			public String getSubjectId()
			{
				return subjectId;
			}

			public void setSubjectId(String subjectId)
			{
				this.subjectId = subjectId;
			}

			public double getConfidence()
			{
				return confidence;
			}

			public void setConfidence(double confidence)
			{
				this.confidence = confidence;
			}

			public String getEnrollmentTimestamp()
			{
				return enrollmentTimestamp;
			}

			public void setEnrollmentTimestamp(String enrollmentTimestamp)
			{
				this.enrollmentTimestamp = enrollmentTimestamp;
			}
		}
	}
}
