package com.vote.shreyas.facialrecognition;

import android.content.Context;
import android.util.Log;

import com.kairos.*;

import org.json.JSONException;

import java.io.UnsupportedEncodingException;

/**
 * Created by Dr.Xperience on 005-2017-07-05.
 */

public class KariosTester
{
	// instantiate a new kairos instance
	Kairos myKairos = new Kairos();

	// set authentication
	String app_id = "86f1aa80";
	String api_key = "06a447e15f223c89055fc8b619fbc393";

	public KariosTester(Context context) throws UnsupportedEncodingException, JSONException
	{
		myKairos.setAuthentication(context, app_id, api_key);
		myKairos.listGalleries(listener);
	}

	public KairosListener listener = new KairosListener()
	{

		@Override
		public void onSuccess(String response)
		{
			// your code here!
			Log.d("KAIROS DEMO", response);
		}

		@Override
		public void onFail(String response)
		{
			// your code here!
			Log.d("KAIROS DEMO", response);
		}
	};

}

