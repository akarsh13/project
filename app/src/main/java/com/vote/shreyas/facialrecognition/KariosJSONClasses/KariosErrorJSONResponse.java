package com.vote.shreyas.facialrecognition.KariosJSONClasses;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Dr.Xperience on 019-2017-09-19.
 */

public class KariosErrorJSONResponse
{

	@SerializedName("Errors")
	private List<Errors> Errors;

	public List<Errors> getErrors()
	{
		return Errors;
	}

	public void setErrors(List<Errors> Errors)
	{
		this.Errors = Errors;
	}

	public static class Errors
	{
		/**
		 * Message : no faces found in the image
		 * ErrCode : 5002
		 */

		@SerializedName("Message")
		private String Message;
		@SerializedName("ErrCode")
		private int ErrCode;

		public String getMessage()
		{
			return Message;
		}

		public void setMessage(String Message)
		{
			this.Message = Message;
		}

		public int getErrCode()
		{
			return ErrCode;
		}

		public void setErrCode(int ErrCode)
		{
			this.ErrCode = ErrCode;
		}
	}
}
