package com.vote.shreyas.facialrecognition;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.internal.Excluder;
import com.kairos.*;
import com.vote.shreyas.akarsh.onlinevotingappova.R;
import com.vote.shreyas.facialrecognition.KariosJSONClasses.KariosErrorJSONResponse;
import com.vote.shreyas.facialrecognition.KariosJSONClasses.KariosFaceRecognizeJSONResponse;

import org.json.JSONException;


import java.io.UnsupportedEncodingException;

public class KariosActivityPrototype extends Activity
{

	/**
	 * Called when the activity is first created.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_face_recognition);

		TextView resp = (TextView) findViewById(R.id.response);
		resp.setText("Inside on Create");
		Log.d("KAIROS DEMO", "onCreate");


		// listener
		KairosListener listenerFaceRecognise = new KairosListener()
		{
			Gson gson = new Gson();
			TextView resp = (TextView) findViewById(R.id.response);

			@Override
			public void onSuccess(String response)
			{
				try
				{
					System.out.println("Success Hu ya");
					Log.d("KAIROS DEMO", response);
					KariosFaceRecognizeJSONResponse json = gson.fromJson(response, KariosFaceRecognizeJSONResponse.class);

					for (KariosFaceRecognizeJSONResponse.Images i : json.getImages())
					{
						resp.setText(resp.getText() + "\n\n Status :: " + i.getTransaction().getStatus());
						int sno = 1;
						for (KariosFaceRecognizeJSONResponse.Images.Candidates c : i.getCandidates())
						{
							resp.setText(resp.getText() + "\n  #" + sno++ + " " + c.getSubjectId() + " : " + c.getConfidence());
						}
					}
				}
				catch (Exception e)
				{
					resp.setText(resp.getText() + "\n\n FaceRecognise Gson parse Error :: " + e.getMessage());
					System.out.println("Gson Exception");
					Log.d("KAIROS DEMO", response);
					e.printStackTrace();
				}

			}

			@Override
			public void onFail(String response)
			{
				try
				{
					KariosErrorJSONResponse error = gson.fromJson(response, KariosErrorJSONResponse.class);
					for (KariosErrorJSONResponse.Errors e : error.getErrors())
					{
						resp.setText(resp.getText() + "\n\n Error :: " + e.getErrCode());
						resp.setText(resp.getText() + "\n :: " + e.getMessage());
					}
					System.out.println("Failure Boo Ha");
//				Log.d("KAIROS DEMO", response);
				}
				catch (Exception e)
				{
					resp.setText(resp.getText() + "\n\n FaceRecognise Gson parse Error :: " + e.getMessage());
					System.out.println("Gson Exception");
					Log.d("KAIROS DEMO", response);
					e.printStackTrace();
				}

			}
		};


        /* * * instantiate a new kairos instance * * */
		Kairos myKairos = new Kairos();

        /* * * set authentication * * */
		String app_id = "86f1aa80";
		String api_key = "06a447e15f223c89055fc8b619fbc393";
		myKairos.setAuthentication(this, app_id, api_key);


		try
		{


            /* * * * * * * * * * * * * * * * * * * * */
			/* * *  Kairos Method Call Examples * * */
			/* * * * * * * * * * * * * * * * * * * */
			/* * * * * * * * * * * * * * * * * * **/


			//  List galleries
			myKairos.listGalleries(listenerFaceRecognise);


            /* * * * * * * * DETECT EXAMPLES * * * * * *


			// Bare-essentials Example:
			// This example uses only an image url, setting optional params to null
//            String image = "http://media.kairos.com/liz.jpg";
//            myKairos.detect(image, null, null, listener);


			// Fine-grained Example:
			// This example uses a bitmap image and also optional parameters
			Bitmap image = BitmapFactory.decodeResource(getResources(), R.drawable.anuenroll);
			String selector = "FULL";
			String minHeadScale = "0.25";
			myKairos.detect(image, selector, minHeadScale, listener);

            /* */



            /* * * * * * * * ENROLL EXAMPLES * * * * * */

			// Bare-essentials Example:
			// This example uses only an image url, setting optional params to null
//			String image = "http://media.kairos.com/liz.jpg";
//			String subjectId = "Elizabeth";
//			String galleryId = "friends";
//			myKairos.enroll(image, subjectId, galleryId, null, null, null, listener);


			// Fine-grained Example:
			// This example uses a bitmap image and also optional parameters
//			Bitmap image = BitmapFactory.decodeResource(getResources(), R.drawable.anuenroll2);
//			String subjectId = "Anubhav";
//			String galleryId = "Dev";
//			String selector = "FULL";
//			String multipleFaces = "false";
//			String minHeadScale = "0.25";
////			myKairos.enroll(image, subjectId, galleryId, selector, multipleFaces, minHeadScale, null);
////			image = BitmapFactory.decodeResource(getResources(), R.drawable.anuenroll3);
////			myKairos.enroll(image, subjectId, galleryId, selector, multipleFaces, minHeadScale, null);
//
//
//                /*    */
//
//
//            /* * * * * * * RECOGNIZE EXAMPLES * * * * * */
//
//			// Bare-essentials Example:
//			// This example uses only an image url, setting optional params to null
////			String image = "http://media.kairos.com/liz.jpg";
////			String galleryId = "friends";
////			myKairos.recognize(image, galleryId, null, null, null, null, listener);
//
//
//			// Fine-grained Example:
//			// This example uses a bitmap image and also optional parameters
//			/*Bitmap*/ image = BitmapFactory.decodeResource(getResources(), R.drawable.anu2);
//			/*String*/ galleryId = "Dev";
//			/*String*/ selector = "FULL";
//			String threshold = "0.75";
//			/*String*/ minHeadScale = "0.25";
//			String maxNumResults = "25";
////			myKairos.recognize(image, galleryId, selector, threshold, minHeadScale, maxNumResults, listenerFaceRecognise);
//			image = BitmapFactory.decodeResource(getResources(), R.drawable.anuenroll3);
//			myKairos.recognize(image, galleryId, selector, threshold, minHeadScale, maxNumResults, listenerFaceRecognise);
//			image = BitmapFactory.decodeResource(getResources(), R.drawable.anu4);
//			myKairos.recognize(image, galleryId, selector, threshold, minHeadScale, maxNumResults, listenerFaceRecognise);

              /* */


            /* * * * GALLERY-MANAGEMENT EXAMPLES * * */


			//  List galleries
//			myKairos.listGalleries(listener);


			//  List subjects in gallery
//			myKairos.listSubjectsForGallery("Dev", listener);
//			myKairos.listSubjectsForGallery("friends", listener);


			// Delete subject from gallery
//			myKairos.deleteSubject("your_subject_id", "your_gallery_name", listener);


			// Delete an entire gallery
//			myKairos.deleteGallery("friends", listener);

            /**/


		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}
		catch (UnsupportedEncodingException e)
		{
			e.printStackTrace();
		}
//		catch (Exception e)
//		{
//			e.printStackTrace();
//		}


	}
}
