package com.vote.shreyas.facialrecognition.KariosJSONClasses;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Dr.Xperience on 019-2017-09-19.
 */

public class KariosFaceVerifyJSONResponse
{

	@SerializedName("images")
	private List<Images> images;

	public List<Images> getImages()
	{
		return images;
	}

	public void setImages(List<Images> images)
	{
		this.images = images;
	}

	public static class Images
	{
		/**
		 * transaction : {"status":"success","subject_id":"Elizabeth","quality":0.84705,"width":170,"height":287,"topLeftX":108,"topLeftY":55,"confidence":0.88309,"gallery_name":"MyGallery"}
		 */

		@SerializedName("transaction")
		private Transaction transaction;

		public Transaction getTransaction()
		{
			return transaction;
		}

		public void setTransaction(Transaction transaction)
		{
			this.transaction = transaction;
		}

		public static class Transaction
		{
			/**
			 * status : success
			 * subject_id : Elizabeth
			 * quality : 0.84705
			 * width : 170
			 * height : 287
			 * topLeftX : 108
			 * topLeftY : 55
			 * confidence : 0.88309
			 * gallery_name : MyGallery
			 */

			@SerializedName("status")
			private String status;
			@SerializedName("subject_id")
			private String subjectId;
			@SerializedName("quality")
			private double quality;
			@SerializedName("width")
			private int width;
			@SerializedName("height")
			private int height;
			@SerializedName("topLeftX")
			private int topLeftX;
			@SerializedName("topLeftY")
			private int topLeftY;
			@SerializedName("confidence")
			private double confidence;
			@SerializedName("gallery_name")
			private String galleryName;

			public String getStatus()
			{
				return status;
			}

			public void setStatus(String status)
			{
				this.status = status;
			}

			public String getSubjectId()
			{
				return subjectId;
			}

			public void setSubjectId(String subjectId)
			{
				this.subjectId = subjectId;
			}

			public double getQuality()
			{
				return quality;
			}

			public void setQuality(double quality)
			{
				this.quality = quality;
			}

			public int getWidth()
			{
				return width;
			}

			public void setWidth(int width)
			{
				this.width = width;
			}

			public int getHeight()
			{
				return height;
			}

			public void setHeight(int height)
			{
				this.height = height;
			}

			public int getTopLeftX()
			{
				return topLeftX;
			}

			public void setTopLeftX(int topLeftX)
			{
				this.topLeftX = topLeftX;
			}

			public int getTopLeftY()
			{
				return topLeftY;
			}

			public void setTopLeftY(int topLeftY)
			{
				this.topLeftY = topLeftY;
			}

			public double getConfidence()
			{
				return confidence;
			}

			public void setConfidence(double confidence)
			{
				this.confidence = confidence;
			}

			public String getGalleryName()
			{
				return galleryName;
			}

			public void setGalleryName(String galleryName)
			{
				this.galleryName = galleryName;
			}
		}
	}
}
