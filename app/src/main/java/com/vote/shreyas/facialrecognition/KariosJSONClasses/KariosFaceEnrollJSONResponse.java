package com.vote.shreyas.facialrecognition.KariosJSONClasses;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Dr.Xperience on 019-2017-09-19.
 */

public class KariosFaceEnrollJSONResponse
{

	/**
	 * face_id : 5410001743ab64935982
	 * images : [{"attributes":{"lips":"Together","asian":0.25658,"gender":{"type":"F"},"age":26,"hispanic":0.41825,"other":0.11144,"black":0.16007,"white":0.05365,"glasses":"None"},"transaction":{"status":"success","topLeftX":390,"topLeftY":706,"gallery_name":"MyGallery","timestamp":"1487012582681","height":780,"quality":0.79333,"confidence":0.99997,"subject_id":"Elizabeth","width":781,"face_id":1}}]
	 */

	@SerializedName("face_id")
	private String faceId;
	@SerializedName("images")
	private List<Images> images;

	public String getFaceId()
	{
		return faceId;
	}

	public void setFaceId(String faceId)
	{
		this.faceId = faceId;
	}

	public List<Images> getImages()
	{
		return images;
	}

	public void setImages(List<Images> images)
	{
		this.images = images;
	}

	public static class Images
	{
		/**
		 * attributes : {"lips":"Together","asian":0.25658,"gender":{"type":"F"},"age":26,"hispanic":0.41825,"other":0.11144,"black":0.16007,"white":0.05365,"glasses":"None"}
		 * transaction : {"status":"success","topLeftX":390,"topLeftY":706,"gallery_name":"MyGallery","timestamp":"1487012582681","height":780,"quality":0.79333,"confidence":0.99997,"subject_id":"Elizabeth","width":781,"face_id":1}
		 */

		@SerializedName("attributes")
		private Attributes attributes;
		@SerializedName("transaction")
		private Transaction transaction;

		public Attributes getAttributes()
		{
			return attributes;
		}

		public void setAttributes(Attributes attributes)
		{
			this.attributes = attributes;
		}

		public Transaction getTransaction()
		{
			return transaction;
		}

		public void setTransaction(Transaction transaction)
		{
			this.transaction = transaction;
		}

		public static class Attributes
		{
			/**
			 * lips : Together
			 * asian : 0.25658
			 * gender : {"type":"F"}
			 * age : 26
			 * hispanic : 0.41825
			 * other : 0.11144
			 * black : 0.16007
			 * white : 0.05365
			 * glasses : None
			 */

			@SerializedName("lips")
			private String lips;
			@SerializedName("asian")
			private double asian;
			@SerializedName("gender")
			private Gender gender;
			@SerializedName("age")
			private int age;
			@SerializedName("hispanic")
			private double hispanic;
			@SerializedName("other")
			private double other;
			@SerializedName("black")
			private double black;
			@SerializedName("white")
			private double white;
			@SerializedName("glasses")
			private String glasses;

			public String getLips()
			{
				return lips;
			}

			public void setLips(String lips)
			{
				this.lips = lips;
			}

			public double getAsian()
			{
				return asian;
			}

			public void setAsian(double asian)
			{
				this.asian = asian;
			}

			public Gender getGender()
			{
				return gender;
			}

			public void setGender(Gender gender)
			{
				this.gender = gender;
			}

			public int getAge()
			{
				return age;
			}

			public void setAge(int age)
			{
				this.age = age;
			}

			public double getHispanic()
			{
				return hispanic;
			}

			public void setHispanic(double hispanic)
			{
				this.hispanic = hispanic;
			}

			public double getOther()
			{
				return other;
			}

			public void setOther(double other)
			{
				this.other = other;
			}

			public double getBlack()
			{
				return black;
			}

			public void setBlack(double black)
			{
				this.black = black;
			}

			public double getWhite()
			{
				return white;
			}

			public void setWhite(double white)
			{
				this.white = white;
			}

			public String getGlasses()
			{
				return glasses;
			}

			public void setGlasses(String glasses)
			{
				this.glasses = glasses;
			}

			public static class Gender
			{
				/**
				 * type : F
				 */

				@SerializedName("type")
				private String type;

				public String getType()
				{
					return type;
				}

				public void setType(String type)
				{
					this.type = type;
				}
			}
		}

		public static class Transaction
		{
			/**
			 * status : success
			 * topLeftX : 390
			 * topLeftY : 706
			 * gallery_name : MyGallery
			 * timestamp : 1487012582681
			 * height : 780
			 * quality : 0.79333
			 * confidence : 0.99997
			 * subject_id : Elizabeth
			 * width : 781
			 * face_id : 1
			 */

			@SerializedName("status")
			private String status;
			@SerializedName("topLeftX")
			private int topLeftX;
			@SerializedName("topLeftY")
			private int topLeftY;
			@SerializedName("gallery_name")
			private String galleryName;
			@SerializedName("timestamp")
			private String timestamp;
			@SerializedName("height")
			private int height;
			@SerializedName("quality")
			private double quality;
			@SerializedName("confidence")
			private double confidence;
			@SerializedName("subject_id")
			private String subjectId;
			@SerializedName("width")
			private int width;
			@SerializedName("face_id")
			private int faceId;

			public String getStatus()
			{
				return status;
			}

			public void setStatus(String status)
			{
				this.status = status;
			}

			public int getTopLeftX()
			{
				return topLeftX;
			}

			public void setTopLeftX(int topLeftX)
			{
				this.topLeftX = topLeftX;
			}

			public int getTopLeftY()
			{
				return topLeftY;
			}

			public void setTopLeftY(int topLeftY)
			{
				this.topLeftY = topLeftY;
			}

			public String getGalleryName()
			{
				return galleryName;
			}

			public void setGalleryName(String galleryName)
			{
				this.galleryName = galleryName;
			}

			public String getTimestamp()
			{
				return timestamp;
			}

			public void setTimestamp(String timestamp)
			{
				this.timestamp = timestamp;
			}

			public int getHeight()
			{
				return height;
			}

			public void setHeight(int height)
			{
				this.height = height;
			}

			public double getQuality()
			{
				return quality;
			}

			public void setQuality(double quality)
			{
				this.quality = quality;
			}

			public double getConfidence()
			{
				return confidence;
			}

			public void setConfidence(double confidence)
			{
				this.confidence = confidence;
			}

			public String getSubjectId()
			{
				return subjectId;
			}

			public void setSubjectId(String subjectId)
			{
				this.subjectId = subjectId;
			}

			public int getWidth()
			{
				return width;
			}

			public void setWidth(int width)
			{
				this.width = width;
			}

			public int getFaceId()
			{
				return faceId;
			}

			public void setFaceId(int faceId)
			{
				this.faceId = faceId;
			}
		}
	}
}
