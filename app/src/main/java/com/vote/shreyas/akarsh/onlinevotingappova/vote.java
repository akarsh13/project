package com.vote.shreyas.akarsh.onlinevotingappova;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.vote.shreyas.facialrecognition.KariosTester;

import static com.vote.shreyas.akarsh.onlinevotingappova.R.styleable.View;

public class vote extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vote);
        final RadioGroup radiogroup =  (RadioGroup) findViewById(R.id.radioGroup);
        Button bt = (Button) findViewById(R.id.button4);

        bt.setOnClickListener(new android.view.View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // get selected radio button from radioGroup
                int selectedId = radiogroup .getCheckedRadioButtonId();

                // find the radio button by returned id
                RadioButton radioButton = (RadioButton) findViewById(selectedId);

                Toast.makeText(vote.this,
                        radioButton.getText(), Toast.LENGTH_SHORT).show();
            }
        });


    }
    int backButtonCount=0;
    public void onBackPressed()
    {
        if(backButtonCount >= 1)
        {
            super.onBackPressed();
            startActivity(new Intent(vote.this, MainActivity.class));
            finish();
            System.exit(1);
        }
        else
        {
            Toast.makeText(this, "Press the back button once again to close the application.", Toast.LENGTH_SHORT).show();
            backButtonCount++;
        }
    }


    }
