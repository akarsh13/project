package com.vote.shreyas.akarsh.onlinevotingappova;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.vote.shreyas.facialrecognition.KariosTester;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener
{

	DrawerLayout drawerLayout;
	ActionBarDrawerToggle drawerToggle;
	NavigationView navigation;
	Button visitbtn;

	@RequiresApi(api = Build.VERSION_CODES.GINGERBREAD)
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);


		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

		FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
		fab.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				Intent intent = new Intent(MainActivity.this, ContactUs.class);
				startActivity(intent);
			}
		});

		DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
		ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
		drawer.setDrawerListener(toggle);
		toggle.syncState();

		NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
		navigationView.setNavigationItemSelectedListener(this);

		initInstances();
		if (!isNetworkAvailable())
		{
			// Create an Alert Dialog
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			// Set the Alert Dialog Message
			builder.setMessage("Internet Connection Required").setCancelable(false).setPositiveButton("Retry", new DialogInterface.OnClickListener()
			{
				public void onClick(DialogInterface dialog, int id)
				{
					// Restart the Activity
					Intent intent = getIntent();
					finish();
					startActivity(intent);
				}
			});
			AlertDialog alert = builder.create();
			alert.show();
		}
		else
		{
			// Locate the button in check_internet_connection.xml
			visitbtn = (Button) findViewById(R.id.button);
			// Set the button visibility
			visitbtn.setVisibility(View.VISIBLE);
			// Capture Button click
			visitbtn.setOnClickListener(new View.OnClickListener()
			{

				@Override

				public void onClick(View arg0)
				{
					// Recheck Network Connection
					if (!isNetworkAvailable())
					{
						AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
						builder.setMessage("Internet Connection Required").setCancelable(false).setPositiveButton("Retry", new DialogInterface.OnClickListener()
						{
							public void onClick(DialogInterface dialog, int id)
							{
								visitbtn.setVisibility(View.GONE);
								// Restart the activity
								Intent intent = new Intent(MainActivity.this, MainActivity.class);
								finish();
								startActivity(intent);

							}

						});
						AlertDialog alert = builder.create();
						alert.show();

					}
					else
					{
						// Open Vote Activity
						EditText uname = (EditText) findViewById(R.id.editText);
						EditText pass = (EditText) findViewById(R.id.editText2);
						if (uname.getText().toString().trim().equals("") || pass.getText().toString().trim().equals(""))
						{

							Dialog dialog = new Dialog(MainActivity.this);
							Toast.makeText(MainActivity.this, "Error!", Toast.LENGTH_LONG).show();
							dialog.dismiss();

							uname.setError("User Name is required!");
							pass.setError("Password is required!");

						}
						else
						{
							EditText UsernameEt = (EditText) findViewById(R.id.editText);
							EditText PasswordEt = (EditText) findViewById(R.id.editText2);
							String username = UsernameEt.getText().toString();
							String password = PasswordEt.getText().toString();
							String type = "login";
							BackgroundWorker backgroundWorker = new BackgroundWorker(MainActivity.this);
							backgroundWorker.execute(type, username, password);

						}
					}

				}
			});
		}
	}

	// Private class isNetworkAvailable
	private boolean isNetworkAvailable()
	{
		// Using ConnectivityManager to check for Network Connection
		ConnectivityManager connectivityManager = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return activeNetworkInfo != null;
	}

	@RequiresApi(api = Build.VERSION_CODES.GINGERBREAD)
	private void initInstances()
	{
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		drawerToggle = new ActionBarDrawerToggle(MainActivity.this, drawerLayout, R.string.hello_world, R.string.hello_world);
		drawerLayout.setDrawerListener(drawerToggle);

		navigation = (NavigationView) findViewById(R.id.nav_view);
		navigation.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener()
		{
			@Override
			public boolean onNavigationItemSelected(MenuItem menuItem)
			{
				int id = menuItem.getItemId();
				switch (id)
				{
					case R.id.item_1:
						//Do some thing here
						// add navigation drawer item onclick method here
						Intent intent = new Intent(MainActivity.this, SignUp.class);
						startActivity(intent);
						break;
					case R.id.item_2:
						//Do some thing here
						// add navigation drawer item onclick method here
						System.exit(1);
						break;
					case R.id.item_3:
						//Do some thing here
						// add navigation drawer item onclick method here
						Intent intent1 = new Intent(MainActivity.this, aboutus.class);
						startActivity(intent1);
						break;
					case R.id.item_4:
						//Do some thing here
						// add navigation drawer item onclick method here
						Intent intent2 = new Intent(MainActivity.this, ContactUs.class);
						startActivity(intent2);
						break;
				}
				return false;
			}
		});

	}

	public void onReset(View v)
	{
		Intent intent = new Intent(this, ResetPassword.class);
		startActivity(intent);
	}

	@Override
	public void onPostCreate(Bundle savedInstanceState)
	{
		super.onPostCreate(savedInstanceState);
		drawerToggle.syncState();
	}


	@Override
	public void onConfigurationChanged(Configuration newConfig)
	{
		super.onConfigurationChanged(newConfig);
		drawerToggle.onConfigurationChanged(newConfig);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main_drawer, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		if (drawerToggle.onOptionsItemSelected(item))
		{
			return true;
		}

		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		//noinspection SimplifiableIfStatement
		if (id == R.string.action_settings)
		{
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	@RequiresApi(api = Build.VERSION_CODES.GINGERBREAD)
	@Override
	public void onBackPressed()
	{
		DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
		if (drawer.isDrawerOpen(GravityCompat.START))
		{
			drawer.closeDrawer(GravityCompat.START);
		}
		else
		{
			super.onBackPressed();
		}
	}


	@RequiresApi(api = Build.VERSION_CODES.GINGERBREAD)
	@SuppressWarnings("StatementWithEmptyBody")
	@Override
	public boolean onNavigationItemSelected(MenuItem item)
	{
		// Handle navigation view item clicks here.
		int id = item.getItemId();

		if (id == R.id.item_1)
		{
			// Handle the camera action
		}
		else if (id == R.id.item_2)
		{

		}
		else if (id == R.id.item_3)
		{

		}
		else if (id == R.id.item_4)
		{

		} //else if (id == R.id.nav_share) {

		//} else if (id == R.id.nav_send) {

		//}

		DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
		drawer.closeDrawer(GravityCompat.START);
		return true;
	}

}
