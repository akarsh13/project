package com.vote.shreyas.akarsh.onlinevotingappova;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class SignUp extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);


    }

    public void signup(View view) {
        EditText usernam = (EditText) findViewById(R.id.editText3);
        EditText pass = (EditText) findViewById(R.id.editText5);
        EditText cnf = (EditText) findViewById(R.id.editText6);
        if (usernam.getText().toString().trim().equals("") || pass.getText().toString().trim().equals("") || cnf.getText().toString().trim().equals("")) {

            Dialog dialog = new Dialog(SignUp.this);
            Toast.makeText(SignUp.this, "Error!", Toast.LENGTH_LONG).show();
            dialog.dismiss();

            usernam.setError("User Name is required!");
            pass.setError("Password is required!");
            cnf.setError("Field can't be Empty!");
        }
         else if(!cnf.getText().toString().equals(pass.getText().toString()) )
            {
                AlertDialog alertDialog = new AlertDialog.Builder(SignUp.this).create();
                alertDialog.setTitle("Oops!");
                alertDialog.setMessage("Passwords do not match");
                alertDialog.show();
            }
         else {
            EditText UsernameEt = (EditText)findViewById(R.id.editText3);
            EditText PasswordEt = (EditText)findViewById(R.id.editText5);
            String username = UsernameEt.getText().toString();
            String password = PasswordEt.getText().toString();
            String type = "register";
            Background background = new Background(this);
            background.execute(type, username, password);



        }
    }
}
