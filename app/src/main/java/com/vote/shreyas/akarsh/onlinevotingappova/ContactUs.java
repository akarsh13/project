package com.vote.shreyas.akarsh.onlinevotingappova;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class ContactUs extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);
    }
    public void onSend(View v){
        EditText txt = (EditText) findViewById(R.id.editText12);
        String s = txt.getText().toString();
        EditText sub = (EditText) findViewById(R.id.editText10);
        String str = sub.getText().toString();
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:"));
        intent.putExtra(Intent.EXTRA_EMAIL, "akarshdeep@gmail.com");
        intent.putExtra(Intent.EXTRA_SUBJECT, str);
        intent.putExtra(Intent.EXTRA_TEXT, s);
        startActivity(intent);
    }
}
